﻿using System;
using System.Collections.Generic;

namespace StringCalculator
{
    public class StringKata
    {
        private const string CustomDelimiterIndicator = "//";

        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }

            var stringNumbersArray = SplitStringNumbersOnDelimiters(numbers);
            var numbersList = FilterNumbers(stringNumbersArray);
            ValidateNumbersArePositive(numbersList);
            
            return Sum(numbersList);
        }

        private string[] SplitStringNumbersOnDelimiters(string numbers)
        {   
            var delimiters = new char[] { ',','\n' };
            
            if (numbers.StartsWith(CustomDelimiterIndicator))
            {
                var numbersAndDelimiters = numbers.Split(delimiters[1], 
                    StringSplitOptions.RemoveEmptyEntries);
                var delimiter = ExtractCustomDelimiters(numbersAndDelimiters);

                return numbersAndDelimiters[1].Split(delimiter, StringSplitOptions.None);
            }
            
            return numbers.Split(delimiters);
        }

        private string[] ExtractCustomDelimiters(string[] numbersAndDelimiters)
        {   
            var bracketsArray = new string[] { "[", "]" };
            var delimiters = numbersAndDelimiters[0].Replace(CustomDelimiterIndicator, string.Empty);

            if (delimiters.StartsWith(bracketsArray[0]) && delimiters.EndsWith(bracketsArray[1]))
            {
                return delimiters.Split(bracketsArray, StringSplitOptions.RemoveEmptyEntries);
            }

            return new string[] {delimiters};
        }

        private List<int> FilterNumbers(string[] stringNumbersArray)
        {
            var numbersListInRange = new List<int>();

            foreach(var number in stringNumbersArray)
            {
                var convertedNumber = int.Parse(number);

                if (convertedNumber < 1001)
                {
                    numbersListInRange.Add(convertedNumber);
                }
            }

            return numbersListInRange;
        }

        private void ValidateNumbersArePositive(List<int> numbersList)
        {
            const string separator = ",";
            const string exceptionMessage = "Negatives not allowed: ";
            var negativeNumbersList = new List<int>();

            foreach(var number in numbersList)
            { 
                if (number < 0)
                {
                    negativeNumbersList.Add(number);
                }
            }

            if (negativeNumbersList.Count > 0)
            {
               throw new Exception(exceptionMessage + string.Join(separator, negativeNumbersList));
            }
        }

        private int Sum(List<int> numbersList)
        {
            int sum = 0; 

            foreach(var number in numbersList)
            {
                sum += number;
            }

            return sum;
        }

    }
}
